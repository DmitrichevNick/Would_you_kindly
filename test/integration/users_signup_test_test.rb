require 'test_helper'

class UsersSignupTestTest < ActionDispatch::IntegrationTest
  test "valid signup information" do
    get sign_up_path
    assert_difference 'User.count', 1 do
      post_via_redirect users_path, user: { name:  "Example User",
                                            email: "user@example.com",
                                            password:              "password",
                                            password_confirmation: "password" }
    end
    assert_template 'users/show'
    assert is_logged_in
  end
end
