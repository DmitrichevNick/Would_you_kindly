Rails.application.routes.draw do
  get 'about' => 'static_pages#about'
  get 'sessions/new'
  get 'static_pages/home'
  get 'static_pages/help'
  get 'sign_up' => 'users#new'
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # You can have the root of your site routed with "root"
  resources :users
  root 'sessions#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  get 'logout'  => 'sessions#destroy'
end
