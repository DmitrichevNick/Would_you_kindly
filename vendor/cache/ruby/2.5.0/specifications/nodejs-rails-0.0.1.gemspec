# -*- encoding: utf-8 -*-
# stub: nodejs-rails 0.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "nodejs-rails".freeze
  s.version = "0.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Jim Weinert".freeze]
  s.date = "2015-08-31"
  s.email = ["james.weinert@gmail.com".freeze]
  s.homepage = "https://github.com/countingtoten/nodejs-rails".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.2.2".freeze)
  s.rubygems_version = "2.7.6.2".freeze
  s.summary = "Gem for installing nodejs".freeze

  s.installed_by_version = "2.7.6.2" if s.respond_to? :installed_by_version
end
